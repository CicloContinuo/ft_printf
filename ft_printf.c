/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/27 19:32:24 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/28 19:34:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdarg.h>

/*
** This function sets a t_fmt_flags to its default values
*/

static void		init_fmt_flags(t_fmt_flags *flags)
{
	if (flags)
	{
		flags->flag_zero = 0;
		flags->flag_left = 0;
		flags->width = 0;
		flags->precision = -1;
		flags->length_h = 0;
		flags->length_hh = 0;
		flags->length_l = 0;
		flags->length_ll = 0;
		flags->type = '\0';
	}
}

/*
** This function processes and displays one formatting mark updating
** the length with the flags options and from the next argument of list ap
*/

void			process_fmt_flags(int *len, va_list ap, t_fmt_flags *flags)
{
	if (flags->type == 'c')
		process_char(len, ap, flags);
	else if (flags->type == 's')
		process_string(len, ap, flags);
	else if (flags->type == 'p')
		process_pointer(len, ap, flags);
	else if (flags->type == 'd' || flags->type == 'i')
		process_signed_decimal(len, ap, flags);
	else if (flags->type == 'u')
		process_unsigned_decimal(len, ap, flags);
	else if (flags->type == 'x')
		process_unsigned_hex_low(len, ap, flags);
	else if (flags->type == 'X')
		process_unsigned_hex_up(len, ap, flags);
	else if (flags->type == '%')
		process_percentage(len, flags);
}

/*
** This function processes a special formating by taking the format string
** starting on the % marker, the arguments list received by ft_printf and
** the length of the output string.
**
** It will parse the formatting string and display its expansion (taking the
** data from ap), updating also the param len. Returns the lenght of the mark.
*/

static int		proc_arg(const char *format, va_list ap, int *len)
{
	t_fmt_flags flags;
	const char	*orig_format;

	orig_format = format;
	init_fmt_flags(&flags);
	format += parse_flags(format, &flags);
	format += parse_width(format, ap, &flags);
	format += parse_precision(format, ap, &flags);
	format += parse_length(format, &flags);
	format += parse_type(format, &flags);
	process_fmt_flags(len, ap, &flags);
	return (format - orig_format);
}

/*
** This function aims to be stdio.h's printf without for loops.
*/

int				ft_printf(const char *format, ...)
{
	va_list	ap;
	int		len;

	len = 0;
	va_start(ap, format);
	while (*format)
	{
		if (*format != '%')
		{
			ft_putchar_fd(*format++, 1);
			len++;
		}
		else
			format += proc_arg(format, ap, &len);
	}
	va_end(ap);
	return (len);
}
