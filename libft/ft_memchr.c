/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 14:51:48 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/08 10:56:03 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

typedef unsigned char	t_byte;

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t	i;
	t_byte	*character;

	character = NULL;
	i = 0;
	while (i < n)
	{
		if ((t_byte)c == ((t_byte*)s)[i])
			return ((t_byte*)s + i);
		i++;
	}
	return (character);
}
